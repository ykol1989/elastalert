FROM python:3.6
ENV PYTHONUNBUFFERED 1
ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN pip install elastalert
RUN pip install "setuptools>=11.3"

WORKDIR /home/elastalert

ADD requirements*.txt ./
RUN pip3 install -r requirements-dev.txt
COPY . /home/elastalert

